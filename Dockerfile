
FROM nginx
COPY . /tmp/.
RUN mkdir /usr/share/nginx/html/site1
RUN mkdir /usr/share/nginx/html/site2
RUN cd /tmp && ls -la
RUN cp -a /tmp/site1/. /usr/share/nginx/html/site1/
RUN cp -a /tmp/site2/. /usr/share/nginx/html/site2/
RUN cp /tmp/infra/nginx.conf /etc/nginx/nginx.conf
WORKDIR /usr/share/nginx/html
EXPOSE 3000
CMD ["nginx","-g","daemon off;"]